#!/bin/bash


# Produce csv file for ContainerWorker: 
# -------------------------------------------------------------
# |ContainerID | AllocatedTime | StartTime | EndTime| HostName|
# -------------------------------------------------------------
function getContainerWorker {
	grep -e "Sending Launch Request for Container with id" -e "Launching Container with Id" -e  "via event C_COMPLETED" -e "Releasing unused container" $1 \
		| awk 'BEGIN {}
	{
	   if ($8 == "Sending") {
	     print $1, $2, $8, $15;	
           } else if ($7 == "Launching") {
             print $1, $2, $7, $11;
           } else if ($6 == "Releasing") { 
             print $1, $2, $6, $9;
           } else {
   	     print $1, $2, $15, $9;		
   	   }
	}
	END {}' \
		| sed 's/, NodeId://g' \
		| sed 's/,/\./g' \
		| awk 'BEGIN {
	}
	{
	   if ($3 == "Sending") {
	     allocated[$4] = $2;
  	     allocatedDay[$4] = $1;	     
	   } else if ($3 == "Launching") {
	     start[$4] = $2;
	     startDay[$4] = $1;
           } else if ($3 == "Releasing") {
	     finish[$4] = $2;
	     finishDay[$4] = $1;
	   } else {
             finish[$4] = $2;
	     finishDay[$4] = $1;
           }
	}
	END {
	   for (i in start) {
	     print i","allocatedDay[i],allocated[i]","startDay[i],start[i]","finishDay[i],finish[i]",";
	   }
	}' \
		> $2/tempDataContainer.csv
	}

# Produce csv file for TaskAttempt:
# ---------------------------------------------------------------------------------------
# |TaskAttemptID | ScheduledTime | PulledTime | CompletedTime | KilledTime | ContainerID|
# ---------------------------------------------------------------------------------------
function getTaskAttempt {
	#grep -e "AsyncDispatcher" $1 \
	grep -e "TaskAttempt Transitioned from NEW to START_WAIT" -e "started. Is using containerId" -e "TaskAttempt Transitioned from RUNNING to SUCCEEDED" \
		-e "TaskAttempt Transitioned from FAIL_IN_PROGRESS to FAILED due to event TA_CONTAINER_TERMINATED" $1 \
		| awk '{if ($10 == "Transitioned") {print $1, $2, $18, $8;} else {print $1, $2, $10, $9, $14;}}' \
		| sed 's/,/\./g' \
		| sed 's/\[//g' \
		| sed 's/\]//g' \
		| awk 'BEGIN {}
	{
	   if ($3 == "TA_SCHEDULE") {
	     scheduled[$4] = $2;
	     scheduledDay[$4] = $1;	     
	   } else if ($3 == "started.") {
	     pulled[$4] = $2;
	     pulledDay[$4] = $1;
	     containerID[$4] = $5;
           } else {
	     completed[$4] = $2;
	     completedDay[$4] = $1;
	   } 
	}
	END {
	   for (i in scheduled) {
	     print i","scheduledDay[i],scheduled[i]","pulledDay[i],pulled[i]","completedDay[i],completed[i]",,"containerID[i];
	     }
	}' \
		> $2/tempDataTaskAttempt.csv
	}


if [[ $# -eq 0 ]]; then
	echo "Error: No arguments"
	exit 1
elif [[ "$1" == "--help" ]] || [[ "$1" == "-h" ]]; then
	python2.7 plotChart.py -h
	exit 1	
elif [[ ! -e "${@: -1}" ]]; then
	echo "Error: The given log file does not exist"
	exit 1
else
	#TIME=$(date +"%Y-%m-%d--%H-%M-%S")
	LOG_FILE=$(basename ${@: -1})
	TEMP="."
	AM_CREATED_TIME=$(grep "Created DAGAppMaster for application" ${@: -1} | awk '{print $1, $2}'|sed 's/,/\./g')
	AM_STOPPED_TIME=$(grep "DAGAppMasterShutdownHook invoked" ${@: -1} | awk '{print $1, $2}'|sed 's/,/\./g')
	#mkdir -p $TEMP
	getContainerWorker ${@: -1} $TEMP
	getTaskAttempt ${@: -1} $TEMP
	if [[ -s $TEMP/tempDataContainer.csv ]]; then
	        python2.7 plotChart.py --am_created_time "$AM_CREATED_TIME" --am_stopped_time "$AM_STOPPED_TIME" --temp_dir "." "$@"	
		rm $TEMP/tempDataContainer.csv $TEMP/tempDataTaskAttempt.csv
	else
		echo "There is no ContainerWorker to visualize"
	fi
	echo "== FINISHED =="
fi
