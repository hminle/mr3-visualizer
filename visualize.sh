#!/bin/bash


# Produce csv file for ContainerWorker: 
# -------------------------------------------------------------
# |ContainerID | AllocatedTime | StartTime | EndTime| HostName|
# -------------------------------------------------------------
function getContainerWorker {
	grep -e "AllocatedCT" -e "RegisteringCT" -e "UnregisteringCT" $1 \
		| sed 's/AllocatedCT container/AllocatedCT Y@container/g' \
	        | sed 's/\[.*\]//g' \
		| awk '{print $1, $2, $6, $7 ,$8}' \
		| sed 's/,/\./g' \
		| awk 'BEGIN {
	}
	{
	   if ($3 == "AllocatedCT") {
	     allocated[$4] = $2;
  	     allocatedDay[$4] = $1;	     
	   } else if ($3 == "RegisteringCT") {
	     start[$4] = $2;
	     startDay[$4] = $1;
	     containerGroupId[$4] = $5;
	   } else {
	     finish[$4] = $2;
	     finishDay[$4] = $1;
	   }
	}
	END {
	   for (i in start) {
	     print i","allocatedDay[i],allocated[i]","startDay[i],start[i]","finishDay[i],finish[i]","containerGroupId[i];
	   }
	}' \
		| sed 's/, /,/g' > $2/tempDataContainer.csv
	}

# Produce csv file for TaskAttempt:
# ---------------------------------------------------------------------------------------
# |TaskAttemptID | ScheduledTime | PulledTime | CompletedTime | KilledTime | ContainerID|
# ---------------------------------------------------------------------------------------
function getTaskAttempt {
	grep -e "ScheduledTA" -e "PulledTA" -e "CompletedTA" -e "KilledTA" -e "notifyScheduled" $1 \
		| sed 's/\[.*\]//g' \
		| awk '{print $1, $2, $6, $7, $8, $9}' \
		| sed 's/,/\./g' \
		| awk 'BEGIN {}
	{
	   if ($3 == "ScheduledTA") {
	     scheduled[$4] = $2;
	     scheduledDay[$4] = $1;	     
	   } else if ($3 == "PulledTA") {
	     pulled[$4] = $2;
	     pulledDay[$4] = $1;
	     containerID[$4] = $5;
           } else if ($3 == "CompletedTA") {
	     completed[$4] = $2;
	     completedDay[$4] = $1;
	   } else if ($3 == "notifyScheduledOnAny" || $3 == "notifyScheduledOnHint") {
	     notifyScheduled[$4] = $3	
	   } else {
             killed[$4] = $2;
	     killedDay[$4] = $1;
           }
	}
	END {
	   for (i in scheduled) {
	     print i","scheduledDay[i],scheduled[i]","pulledDay[i],pulled[i]","completedDay[i],completed[i]","killedDay[i],killed[i]","containerID[i]","notifyScheduled[i];
	     }
	}' \
		| sed 's/, /,/g' > $2/tempDataTaskAttempt.csv
	}


if [[ $# -eq 0 ]]; then
	echo "Error: No arguments"
	exit 1
elif [[ "$1" == "--help" ]] || [[ "$1" == "-h" ]]; then
	python2.7 plotChart.py -h
	exit 1	
elif [[ ! -e "${@: -1}" ]]; then
	echo "Error: The given log file does not exist"
	exit 1
else
	#TIME=$(date +"%Y-%m-%d--%H-%M-%S")
	LOG_FILE=$(basename ${@: -1})
	#TEMP=$LOG_FILE.out
	TEMP="."
	AM_CREATED_TIME=$(grep "CreatedDAGAppMaster" ${@: -1} | awk '{print $1, $2}'|sed 's/,/\./g')
	AM_STOPPED_TIME=$(grep "StoppedDAGAppMaster" ${@: -1} | awk '{print $1, $2}'|sed 's/,/\./g')
	#mkdir -p $TEMP
	getContainerWorker ${@: -1} $TEMP
	getTaskAttempt ${@: -1} $TEMP
        if [[ -s $TEMP/tempDataContainer.csv ]]; then
                python2.7 plotChart.py --am_created_time "$AM_CREATED_TIME" --am_stopped_time "$AM_STOPPED_TIME" --temp_dir "." "$@" 
                rm $TEMP/tempDataContainer.csv $TEMP/tempDataTaskAttempt.csv
        else
                echo "There is no ContainerWorker to visualize" 
        fi      
	echo "== FINISHED =="
fi
