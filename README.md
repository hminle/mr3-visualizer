## Requirements:
- Python2.7
- Python Packages: matplotlib, numpy, pandas (using pip to install)

## Usage:
./visualize.sh [-h] [-c] [-p <NumOfProcesses>] [-x <NumOfXticks>]
                      log_file

Visualize mr3/tez log

positional arguments:
  log_file              Input Log File

optional arguments:
  -h, --help            show this help message and exit
  -c, --container       Option to show an image for each container,
                        default=False
  -p <NumOfProcesses>, --parallel <NumOfProcesses>
                        Number of processes, default=1
  -x <NumOfXticks>, --xticks <NumOfXticks>
                        Number of minor ticks on x-axis, default=10
  -o OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER
			Output folder. Required
  -s SORTINGOPTIONFORTA, --sortingOptionForTA SORTINGOPTIONFORTA
                        1: ScheduledTime 2: PulledTime 3: VertexId Default:
                        PulledTime
